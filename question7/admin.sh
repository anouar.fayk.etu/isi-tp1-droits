#!/bin/bash

su administer
echo "-------SCRIPT ADMINISTER-------"
echo "Lecture de dir_a et le fichier qu'il contient"
ls dir_a
echo
echo "Lecture de file3 dans dir_a"
cat dir_a/file3
echo
echo "Supprimer file3 dans dir_a"
rm dir_a/file3
echo
echo "Création d'un nouveau fichier file_a"
touch dir_a/file_a
echo -e "I'm admin" >>dir_a/file_a
echo
ls dir_a
cat dir_a

echo "Lecture de dir_b et le fichier qu'il contient"
ls dir_b
echo
echo "Lecture du file3 dans dir_b"
cat dir_b/file3
echo
echo "supprimer file3 dans dir_b"
rm dir_b/file3
echo
echo "Création d'un nouveau fichier file_b"
touch dir_b/file_b
echo -e "I'm admin" >>dir_b/file_b
echo
ls dir_b
cat dir_b

echo "Lecture de dir_c et le fichier qu'il contient"
ls dir_c
echo
echo "Lecture du file3 dans dir_c"
cat dir_c/file3
echo
echo "supprimer file3 dans dir_c"
rm dir_c/file3
echo
echo "Création d'un nouveau fichier file_c"
touch dir_c/file_c
echo -e "I'm admin" >>dir_c/file_c
echo
ls dir_c
cat dir_c
