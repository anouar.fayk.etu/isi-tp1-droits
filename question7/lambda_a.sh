#!/bin/bash
su lambda_a
echo "-------SCRIPT LAMBDA_A------"
echo "Lecture de dir_a et le fichier qu'il contient"
ls dir_a
echo
echo "Lecture du file1 dans dir_a"
cat dir_a/file1
echo
echo "J'essaierai de supprimer file1 dans dir_a mais je n'ai pas la permission"
rm dir_a/file1
echo
echo "Création d'un nouveau fichier file_a dans dir_a"
touch dir_a/file_a
ls dir_a
echo -e "I'm lambda_a" >>dir_a/file_a
echo
echo "Lecture du lire file_a"
cat dir_a/file_a
echo
echo "J'ai supprimé file_a et cela a réussi"
rm dir_a/file_a
echo
echo "Lecture de lire dir_c et le fichier qu'il contient"
ls dir_c
echo
echo "Lecture du file1 dans dir_c"
cat dir_c/file1
echo
echo "J'essaierai de supprimer file1 dans dir_c mais je n'ai pas la permission"
rm dir_c/file
echo
echo "J'essaierai créer un nouveau fichier file_a dans dir_c mais je n'ai pas la permission"
touch dir_c/file_a
echo
echo "J'essaierai de lire dir_b mais je n'ai pas la permission"
ls dir_b
