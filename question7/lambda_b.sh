#!/bin/bash
su lambda_b
echo "---------SCRIPT LAMBDA_B--------"

echo "Lecture de dir_b et le fichier qu'il contient"
ls dir_b
echo
echo "Lecture du file2 dans dir_b"
cat dir_b/file
echo
echo "J'essaierai de supprimer file2 dans dir_b mais je n'ai pas la permission"
rm dir_b/file
echo
echo "Création un nouveau fichier file_b dans dir_b"
touch dir_b/file_b
ls dir_b
echo -e "I'm lambda_b" >>dir_b/file_b
echo
echo "Lecture file_b dans dir_b"
cat dir_b/file_b
echo
echo "J'ai supprimé file_b dans dir_b et cela a réussi"
rm dir_b/file_b
echo
echo "Lecture de dir_c et le fichier qu'il contient"
ls dir_c
echo
echo "Lecture du file2 dans dir_c"
cat dir_c/file2
echo
echo "J'essaierai de supprimer file2 dans dir_c mais je n'ai pas la permission"
rm dir_c/file2
echo
echo "J'essaierai de créer un nouveau fichier file_b mais je n'ai pas la permission"
touch dir_c/file_b
echo
echo "J'essaierai de lire dir_a mais je n'ai pas la permission"
ls dir_a
