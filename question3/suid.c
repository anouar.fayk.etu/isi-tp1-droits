#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
  char buff[255];
  FILE *f;

  printf("EUID is %d\n", geteuid());
  printf("EGID is %d\n", getegid());
  printf("RUID is %d\n", getuid());
  printf("RGID is %d\n", getgid());

  f =  fopen("mydir/data.txt","r");

  if(f == NULL){
  printf("Cannot open file \n");
  exit(0);
  }

  while(fgets(buff, 255, (FILE*)f)!=NULL){
    printf("%s\n", buff );
  }

  fclose(f);
  return 0;

}
