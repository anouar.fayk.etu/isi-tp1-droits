#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

//Le format de stockage de l'identifiant et du mot de passe dans le fichier est
//uid:password
//Le mot de passe comporte six chiffres

#define PATH "./home/admin/passwd"
#define PWD_SIZE 6

//return le uid de la chaîne stockée dans le buf
int get_id(char* buf){
  int i = 0;
  char id[50];
  while(buf[i]!='\0'){
    if(buf[i]!=':'){
      id[i]=buf[i];
      i++;
    }else{
      break;
    }
  }
  return atoi(id);
}

//return le mot de passe de la chaîne stockée dans le buf
char * get_pwd(char *buf){
  int i,j;
  i=0;
  char* pwd;
  pwd = (char*)malloc(PWD_SIZE*sizeof(char));

  while(buf[i]!='\0'){
    if(buf[i]!=':'){
      i++;
    }else{
      break;
    }
  }

  for (int j = 0; j < PWD_SIZE; j++) {
    pwd[j]=buf[(j+i+1)];
  }
  return pwd;
}

//Vérifiez si le mot de passe saisi est correct
//si correct,return 1 ;sinon, return 0
int check_password(char* pwd_input){
  char ch;
  char buff[255];
  FILE *f;
  char *pwd;
  int i;
  int uid;
  int id;

  pwd = (char*)malloc(PWD_SIZE*sizeof(char));

  uid = getuid();
  f =  fopen(PATH,"r");

  if(f == NULL){
  printf("Cannot open file \n");
  exit(1);
  }

  i=0;
  while ((ch=fgetc(f)) != EOF){
    if (ch == '\n'){
      buff[i]='\0';
      i=0;
      id = get_id(buff);
      if(id==uid){
        pwd = get_pwd(buff);
        if (!strcmp(pwd,pwd_input)){
          return 1;
        }
      }

    }else{
      buff[i] = ch;
      i++;
    }
  }
  fclose(f);
  return 0;
}
