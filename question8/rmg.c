#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>
#include "check_pass.h"

//Si l'utilisateur fait partie du groupe auquel appartient le fichier
//et que le mot de passe est entré correctement,
//supprimez le fichier


int main(int argc, char const *argv[]) {

  const char *filename;
  int usr_gid;
  int file_gid;
  int status;
  struct stat st;
  char password[30];


  if (argc!=2) {
    perror("Input error: Please enter the file name to be deleted");
    exit(1);
  }

  filename = argv[1];
  if(lstat(filename,&st)){
    perror("Cannot find filessss");
    exit(1);
  }

  file_gid = st.st_gid;
  usr_gid = getgid();
  if (file_gid==usr_gid) {
    printf("Please enter password : \n");
    scanf("%s",password);

    if(check_password(password)){
      unlink(filename);
      printf("File has been removed\n");
    }else{
      perror("Permission Error: Wrong password");
    }

  }else{
    perror("Permission Error: User and file does not belongs to the same group");
    exit(1);
  }

  return 0;

}
