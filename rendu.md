# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- FAYK, ANOUAR, email:anouar.fayk.etu@univ-lille.fr

- TERMECHE, ADEL, email:adel.termeche.etu@univ-lille.fr

## Question 1

la création un utilisateur toto dans le système, et ajoutez-le au groupe ubuntu:

adduser toto --ingroupe ubuntu 

-r--rw-r-- 1 toto ubuntu 5 Jan 5 09:35 titi.txt

Le processus ne peut pas écrire dans titi.txt, car toto est son propriétaire n'a pas des permissions écriture.

## Question 2

- Le caractère 'x' signifie qu'un répertoire doit être exécutable pour que vous puissiez accéder à l'ensemble de ses sous-répertoires. 
- Le groupe d'utilisateurs ubuntu n'a pas le droit d'accès au répertoire mydir,et sait que toto fait partie du groupe toto et n'a donc pas le droit accès au répertoire.
- Le groupe d'utilisateurs ubuntu où se trouve toto n'a pas l'autorisation d'exécuter le répertoire, il ne peut donc pas accéder à ses sous-répertoires, de plus la sortie de ls nous donne les détails mais on obtient seulement les noms des élements tout le reste est caché (avec des pts d'interrogation). C'est normal car toto n'a pas le droit de parcourir ce fichier.

## Question 3

- les valeurs des différents ids aprés l'éxecution de programme sont :

	EUID: 1001
	EGID: 1001
	RUID: 1001
	RGID: 1001

Le processus n'arrive pas à ouvrir le fichier , on obtient l'erreur : "Cannot open file : Permission denied".

- Après activation du flag set-user-id, on obtient les IDs suivants :

	EUID is 1000
	EGID is 1001
	RUID is 1001
	RGID is 1001

Et cette fois le programme arrive a lire le contenu du fichier.


## Question 4

Les valeurs des différents ids aprés l'éxecution du suid.py sont :
	EUID : 1001
	EGID : 1001

## Question 5

- la commande chfn permet de change le nom et les informations d'un utilisateur.
- la commande : ls -al /usr/bin/chfn à les droits suivant :
**-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn**
Le fichier est lisable pour tout le monde, seul le propriétaire(root) dispose des autorisations d'écriture et d'exécution.
Le caractère s indique la permission d’exécution avec set-user-id. Ainsi, d'autres utilisateurs peuvent également exécuter le fichier

Avant commande chfn :
toto:x:1001:1001:toto,,,:/home/toto:/bin/bash
Après modification :
toto:x:1001:1001:toto,1,0123456789,0987654321,other:/home/toto:/bin/bash

## Question 6

Les mots de passes sont dans /etc/shadow car cela permet d'avoir une sécurité grace au fait que les mots de passes soient cryptés ainsi que grace au fait que /etc/shadow ne soit accessible que par root contrairement à /etc/passwd .

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








